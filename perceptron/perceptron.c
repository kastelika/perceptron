#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "../calculations/calculations.h"

#define POINTS_COUNT 5

double Y[POINTS_COUNT][2] = {
    { -8.167, -9.44 },
    { -15.32, -3.714 },
    { 6.034, -13.297 },
    { -9.875,	5.244 },
    { -19.479, -11.777 },
};

double Z[POINTS_COUNT][2] = {
    { 13.083,	21.09 },
    { 8.882,	31.55 },
    { 14.4,	2.97      },
    { -5.403,	5.514 },
    { -16.498,	14.229}
};


#define FSR_POINT_CNT 10

double fsr[FSR_POINT_CNT][2] = {
    {1.1944 ,-2.8432   },
    {-5.9586,	2.8828 },
    {15.3954 ,-6.7002  },
    {-0.5136,	11.8408},
    {-10.1176, -5.1802} ,
    {10.1902,	6.0194 },
    {5.9892,	16.4794},
    {11.5072, -12.1006 },
    {-8.2958, -9.5566 } ,
    {-19.3908, -0.8416}
};

double random();
double calcParam(double* _weights, double* _points);
void calcError(double* _error, double* _classificationList);
void calcWeights(double* _weights, double _alpha, double* _error);
void printArray(double** _arr, int _n, int _m);

int main(int argc, char* argv[]) {
    srand(time(0));

    int i = 0;
    double w0 = 1.0;
    double w1 = -1.0;
    double w2 = -1.273;

    double** tab = malloc(FSR_POINT_CNT * sizeof(double*));
    for (i = 0; i < FSR_POINT_CNT; ++i) {
        tab[i] = malloc(2 * sizeof(double));
        tab[i][0] = fsr[i][0];
        tab[i][1] = fsr[i][1];
    }

    double mean[2] = { 8.0, 10.0 };
    double omega[2] = { 0.0, 0.0 };

    CalcFisher(tab, FSR_POINT_CNT, mean, omega);
    for (i = 0; i < FSR_POINT_CNT; ++i)
        free(tab[i]);
    free(tab);
    //double w0 = random();
    //double w1 = random();
    //double w2 = (-w0 - w2*points(1, 2)) / points(1, 1);

    double error[3] = { 0 };
    double weights[3] = { w0, w1, w2 };
    double weights2[3] = { w0, w1, w2 };

    int isMissclassifiedExists = 1;
    double** classificationList = malloc(POINTS_COUNT * 2 * sizeof(double*));

    double alpha = 0.3;
    int counter = 0;

    for (i = 0; i < POINTS_COUNT; ++i) {
        classificationList[i] = malloc(4 * sizeof(double));
        classificationList[i][0] = Y[i][0];
        classificationList[i][1] = Y[i][1];
        classificationList[i][2] = 1;
    }
    for (i = POINTS_COUNT; i < 2 * POINTS_COUNT; ++i) {
        classificationList[i] = malloc(4 * sizeof(double));
        classificationList[i][0] = -Z[i - POINTS_COUNT][0];
        classificationList[i][1] = -Z[i - POINTS_COUNT][1];
        classificationList[i][2] = -1;
    }

    int totalIterations = CalcPerceptron(classificationList, POINTS_COUNT * 2, weights2);

    while (isMissclassifiedExists) {
        isMissclassifiedExists = 0;

        for (i = 0; i < 2 * POINTS_COUNT; ++i) {
            classificationList[i][3] = calcParam(weights, classificationList[i]);
            if (classificationList[i][3] < 0) {
                calcError(error, classificationList[i]);
                isMissclassifiedExists = 1;
            }
        }

        calcWeights(weights, alpha, error);

        if (++counter > 100000)
            isMissclassifiedExists = 0;
    }
    printf("classificationList:\n");
    printArray(classificationList, 2 * POINTS_COUNT, 4);
    printf("\nweights:\n");
    double* pdWeights = &weights;
    printArray(&pdWeights, 1, 3);

    for (i = 0; i < 2 * POINTS_COUNT; i++)
        free(classificationList[i]);

    free(classificationList);
    return 0;
}

double random() {
    return (double)rand() / RAND_MAX;
}

double calcParam(double* _weights, double* _points) {
    return
        *_weights * *_points
        + *(_weights + 1) * *(_points + 1)
        + *(_weights + 2) * *(_points + 2);
}

void calcError(double* _error, double* _classificationList) {
    *_error += *_classificationList;
    *(_error + 1) += *(_classificationList + 1);
    *(_error + 2) += *(_classificationList + 2);
}

void calcWeights(double* _weights, double _alpha, double* _error) {
    *_weights += *_error * _alpha;
    *(_weights + 1) += *(_error + 1) * _alpha;
    *(_weights + 2) += *(_error + 2) * _alpha;
}


void printArray(double** _arr, int _n, int _m) {
    int i = 0;
    int j = 0;
    for (i = 0; i < _n; ++i) {
        double* tmpRef = *(_arr + i);
        for (j = 0; j < _m; ++j) {
            printf("%f\t", *(tmpRef + j));
        }
        printf("\n");
    }
}