#ifndef _CALCULATIONS_H_
#define _CALCULATIONS_H_

# ifdef CALCULATIONS_EXPORTS
#  define DLLEXPORT __declspec(dllexport)
# else
#  define DLLEXPORT _stdcall
//#  define DLLEXPORT __declspec(dllimport)
# endif

# ifdef __cplusplus
extern "C" {
# endif

int DLLEXPORT MyProc1(int x, int y);
int DLLEXPORT MyProc2(int* x);
int DLLEXPORT MyProc3(double** x);
int DLLEXPORT IsPositive(double* x);
int DLLEXPORT CalcPerceptron(double** arr_x, int x_len, double* res);
int DLLEXPORT CalcFisher(double** arr_x, int x_len, double* mean, double* res);

# ifdef __cplusplus
}
# endif

#endif
