.686
.model flat, stdcall
.data
    iter_count EQU 100000
    is_blabla EQU 40000000h
    not_blabla EQU not is_blabla
    
    sign_bit EQU 80000000h

    o_cntr EQU 4

    c_2 DWORD 2
    errors REAL8 0.0, 0.0, 0.0
    ;errors REAL8 1.0, 2.0, 3.0
    alpha REAL8 0.3
.code

CalcPerceptron proc arr_x: PTR DWORD, x_len: DWORD, res: PTR REAL8
        finit
        fldz
        fst [errors]
        fst [errors + 8]
        fstp [errors + 16]
        mov edi, res
        
        xor eax, eax            ; eax = eax ^ eax;
C1:     or eax, is_blabla
        mov ebx, arr_x
        
        ; for loop
        xor ecx, ecx
C2:     cmp ecx, x_len                              ; begin for1 loop
        jge EC2
        
        mov esi, [ebx]
        
        fld REAL8 PTR [esi + 0]
        fld REAL8 PTR [edi + 0]
        fmulp
        fld REAL8 PTR [esi + 8]
        fld REAL8 PTR [edi + 8]
        fmulp
        faddp
        fld REAL8 PTR [esi + 16]
        fld REAL8 PTR [edi + 16]
        fmulp
        faddp
        fstp REAL8 PTR [esi + 24]
        ; fcomip zero_f
        mov edx, DWORD PTR [esi + 28]
        and edx, sign_bit
        cmp edx, 0
        je C3
        
        and eax, not_blabla
        fld REAL8 PTR [errors]
        fld REAL8 PTR [esi + 0]
        faddp
        fstp REAL8 PTR [errors]

        fld REAL8 PTR [errors + 8]
        fld REAL8 PTR [esi + 8]
        faddp
        fstp REAL8 PTR [errors + 8]

        fld REAL8 PTR [errors + 16]
        fld REAL8 PTR [esi + 16]
        faddp
        fstp REAL8 PTR [errors + 16]
C3:        
        add ebx, 4
        inc ecx
        jmp C2

EC2:                                                ; end for1 loop
        fld alpha
        fld REAL8 PTR [errors]
        fmul ST(0), ST(1)
        fld REAL8 PTR [edi]
        faddp
        fstp REAL8 PTR [edi]
        
        fld REAL8 PTR [errors + 8]
        fmul ST(0), ST(1)
        fld REAL8 PTR [edi + 8]
        faddp
        fstp REAL8 PTR [edi + 8]
        
        fld REAL8 PTR [errors + 16]
        fmul ST(0), ST(1)
        fld REAL8 PTR [edi + 16]
        faddp
        fstp REAL8 PTR [edi + 16]
        fstp ST(0)

        inc eax
        cmp eax, iter_count
        jl C1
        
        and eax, not_blabla
        ret
CalcPerceptron endp

CalcFisher proc arr_x: PTR DWORD, x_len: DWORD, mean: PTR REAL8, res: PTR REAL8
        finit
        mov ebx, arr_x
        xor ecx, ecx
        sub esp, 24         ; allocate 3 doubles
        fldz
        fst REAL8 PTR [esp]
        fst REAL8 PTR [esp + 8]
        fstp REAL8 PTR [esp + 16]

C4:     cmp ecx, x_len
        jge EC4
        mov esi, [ebx]
        
        fld REAL8 PTR [esi]
        fmul ST(0), ST(0)
        fld REAL8 PTR [esp]
        faddp
        fstp REAL8 PTR [esp]

        fld REAL8 PTR [esi]
        fld REAL8 PTR [esi + 8]
        fmulp
        fld REAL8 PTR [esp - 8]
        faddp
        fstp REAL8 PTR [esp - 8]

        fld REAL8 PTR [esi + 8]
        fmul ST(0), ST(0)
        fld REAL8 PTR [esp - 16]
        faddp
        fstp REAL8 PTR [esp - 16]

        add ebx, 4
        inc ecx
        jmp C4
EC4:
        ; sub esp, 4
        ; mov edx, 
        ; shr [esp - 4], x_len

        ; xor edx, edx
        ; mov eax, x_len
        ; idiv c_2

        fild x_len
        fild x_len
        fild c_2
        fsubp
        fild c_2
        fmulp
        fdivp
        ; pft
        
        fld REAL8 PTR [esp]
        fmul ST(0), ST(1)
        fstp REAL8 PTR [esp]
        
        fld REAL8 PTR [esp - 8]
        fmul ST(0), ST(1)
        fstp REAL8 PTR [esp - 8]
        
        fld REAL8 PTR [esp - 16]
        fmul ST(0), ST(1)
        fstp REAL8 PTR [esp - 16]

        fstp ST(0)

        ; determinant x
		fld REAL8 PTR [esp]             ; cell1
        fld REAL8 PTR [esp - 16]        ; cell3
        fmulp
        fld REAL8 PTR [esp - 8]         ; cell2
        fmul ST(0), ST(0)		
        fsubp
		; fst REAL8 PTR [edi + 24]


        ; upgrade a b c
		fld REAL8 PTR [esp]  ; cell1
		fdiv ST(0), ST(1)
		fstp REAL8 PTR [esp]
		

		fld REAL8 PTR [esp - 8]  ; cell2
		fdiv ST(0), ST(1)
		; fmul ST(0), -1.0
        fchs
		fstp REAL8 PTR [esp - 8]
		
		
		fld REAL8 PTR [esp - 16] ; cell3
		fdiv ST(0), ST(1)
		fstp REAL8 PTR [esp - 16]
	    fstp ST(0)

        ; ====== final vector calculation ===========
        mov edx, mean
        mov eax, res
		fld REAL8 PTR [esp - 16] ;cell3
		fld REAL8 PTR [edx] ; meanx
		fmulp
		fld REAL8 PTR [esp - 8] ;cell2
		fld REAL8 PTR [edx + 8] ;meany
		fmulp
		fadd
		fst REAL8 PTR [eax]
		
		
		fld REAL8 PTR [esp - 8] ; cell2
		fld REAL8 PTR [edx] ; meanx
		fmulp
		fld REAL8 PTR [esp]     ; cell1 
		fld REAL8 PTR [edx + 8] ; meany
		fmulp
		fadd
		fst REAL8 PTR [eax + 8]

        ; add esp, 4
        add esp, 24
        ret
CalcFisher endp


MyProc1 proc x: DWORD, y: DWORD
     xor eax,eax
     mov eax,x
     mov ecx,y
     ror ecx,1
     shld eax,ecx,2
     jnc ET1
     mul y
     ret

ET1: mul x
     neg y
     ret
MyProc1 endp

MyProc2 proc x: DWORD
     mov ecx, DWORD PTR x
     xor ebx, ebx
     xor eax, eax
LOP: 
     cmp ebx, 10
     jge EOP
     ;add eax, [ecx + ebx*4]
     mov [ecx + ebx*4], ebx
     inc ebx
     jmp LOP
EOP:
     ret
MyProc2 endp

MyProc3 proc x: PTR DWORD
        mov eax, x
        mov eax, [eax]
        fld1
        fld1
        faddp
        fldpi
        fmulp
        fstp REAL8 PTR [eax]
        ret

MyProc3 endp

IsPositive proc x: PTR REAL8
        mov esi, x
        finit
        fld REAL8 PTR [esi]
        fldz
        faddp

        fld1
        fldz
        faddp

        fldpi
        fmulp

        faddp
        
        fldpi
        fsubp
        
        fstp REAL8 PTR [esi]
        mov edx, DWORD PTR [esi + 4]
        and edx, sign_bit
        cmp edx, 0
        je C5
        mov eax, 0
        ret
C5:     mov eax, 1
        ret
IsPositive endp

end